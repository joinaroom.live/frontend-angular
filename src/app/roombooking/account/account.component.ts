import { Component, Input, OnInit } from '@angular/core';
import { User } from '../../shared/model/user';
import { FormGroup } from '@angular/forms';
import { PaymentService } from '../../shared/service/payment.service';
import { PayRequest } from '../../shared/model/pay-request';
import { Rooms } from '../../shared/model/rooms';
import { AuthService } from '../../shared/service/auth/auth.service';
import * as moment from 'moment';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { LoginComponent } from '../../auth/login/login.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { RoomService } from '../../shared/service/room/room.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-account',
    templateUrl: './account.component.html',
    styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

    currentUser: User;
    room: Rooms;
    form: FormGroup;
    userOrRoomEmpty: boolean;
    paid: boolean;
    roomPassed: boolean;
    isNotFree: boolean;

    loading = false;
    startTime: string;
    endTime: string;
    eventDate: string;


    constructor(
        private dialog: MatDialog,
        private sanckBar: MatSnackBar,
        private paymentService: PaymentService,
        private authService: AuthService,
        private roomService: RoomService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private snackBar: MatSnackBar
    ) {
        this.getUser();
    }

    private getUser() {
        this.authService.currentUser$.subscribe((data) => {
            this.currentUser = data;
        });
    }

    ngOnInit(): void {
        this.activatedRoute.params.subscribe(params => {
            this.activatedRoute = params.token;
            if (params.id != null) {
                this.userOrRoomEmpty = false;
                this.getRoom(params.id);
            } else {
                this.userOrRoomEmpty = true;
            }
        });
        if (this.currentUser !== null && this.room !== null) {
            this.checkIfRoomIPaid();
        }
    }

    private checkIfRoomIPaid() {
        this.loading = true;
        this.roomService
            .getAllRoomByUser()
            .subscribe(
                (data) => {
                    data.forEach(cat => {
                        const rooms = cat.events.filter(room => room.user.id === this.currentUser.sub);
                        this.paid = rooms.filter(room => room?.id === this.room?.id).length >= 1;
                    });
                    this.loading = false;
                },
                (error) => {
                    this.paid = false;
                    this.loading = false;
                },
            );

    }

    private setTime() {
        this.startTime = moment.unix(this.room.startTime / 1000).format('LT');
        this.endTime = moment.unix(this.room.endTime / 1000).format('LT');
        this.eventDate = moment(this.room.eventDate).format('ll');
        this.roomPassed = this.room.endTime / 1000 < moment().unix();
    }

    private getRoom(id: number) {
        this.loading = true;
        this.roomService.getRoomById(id)
            .subscribe(data => {
                this.loading = false;
                this.room = data;
                this.isNotFree = this.room.price > 0;
                console.log(this.isNotFree);
                this.setTime();
                this.userOrRoomEmpty = false;
            },
                (error) => {
                    this.loading = false;
                    this.userOrRoomEmpty = true;
                },
            );
    }


    requestPayment() {
        if (this.currentUser == null) {
            this.showLoginDialog();
        } else if (this.paid) {
            this.startRoom(this.room);
        } else {
            this.sendRequest();
        }

    }

    startRoom(room: Rooms) {
        if (this.currentUser?.sub) {
            window.location.href = `${window.location.origin}/video/${this.currentUser.sub}/${room.id}`;
        }
    }
    showMessage(message, panelClass) {
        this.snackBar.open(message, 'Ok',
            {
                duration: 2000,
                verticalPosition: 'top',
                panelClass: [panelClass]
            });
    }

    deleteEvent() {
        const confirmDelete = confirm('Are yuo sure you want to delete this event?');
        if (confirmDelete) {
            this.roomService.deleteRoom(this.room.id).subscribe(res => {
                this.showMessage('Event deleted successfully', 'my-snackbar-success');
                this.router.navigate(['/all-rooms']);
            }, err => {
                this.showMessage('Something went wrong. Please try again.', 'my-snackbar-error');
                console.log(err);
            });
        }
    }

    private sendRequest() {
        this.loading = true;
        const paymentRequest = {} as PayRequest;
        paymentRequest.provider = 'paypal';
        paymentRequest.eventId = this.room.id;
        paymentRequest.userEmail = this.currentUser.email;
        paymentRequest.amountToPay = this.room.price;
        this.paymentService.paymentRequest(paymentRequest)
            .subscribe(
                (data: any) => {
                    this.loading = false;
                    sessionStorage.setItem('OrderId', data.paypalOrderId);
                    const isNewUser = this.checkUserSession(data.email);
                    if (isNewUser) {
                        window.location.href = environment.paypalUrl + '/checkoutweb/logout?token=' + data.paypalOrderId + '&reason=logout';
                    } else {
                        window.location.href = data.approvalUrl;
                    }
                },
                (error) => {
                    this.loading = false;
                    console.log(error.message);
                },
            );
    }

    private checkUserSession(userEmail) {
        const oldSessionEmail = sessionStorage.getItem('userEmail');
        if (oldSessionEmail === userEmail) {
            return false;
        } else {
            sessionStorage.setItem('userEmail', userEmail);
            return true;
        }
    }

    private showLoginDialog() {
        const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.data = this.room;
        if (window.innerWidth < 576) {
            dialogConfig.panelClass = 'my-full-screen-dialog';
        } else {
            dialogConfig.panelClass = 'custom-dialog-container';
        }
        let dialogRef;
        dialogRef = this.dialog.open(LoginComponent, dialogConfig);
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                window.location.href = `${window.location.origin}/rooms/${this.room.id}`;
                this.sanckBar.open('Login Success', 'Ok', {
                    duration: 3000
                });
            }
        });
    }

    goHome() {
        window.location.href = `${window.location.origin}/home#rooms`;
    }

    joinRoom() {
        window.open(`${window.location.origin}/video/${this.currentUser.sub}/${this.room.id}`, '_blank');
    }
}
