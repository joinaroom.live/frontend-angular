import {AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2, ViewChild} from '@angular/core';
import {
  RemoteTrack,
  RemoteAudioTrack,
  RemoteVideoTrack,
} from 'twilio-video';

@Component({
  selector: 'app-speaker-view',
  templateUrl: './speaker-view.component.html',
  styleUrls: ['./speaker-view.component.css']
})
export class SpeakerViewComponent {

  @ViewChild('preview', {static: false}) preview: ElementRef;
  @ViewChild('ScreenTrack', {static: false}) ScreenTrack: ElementRef;

  isPreviewing = true;
  localInitial: string;
  videoTrack;

  @Input() track: any;

  @Input('participant')
  set participant(val: any) {
    this.setData(val);
  }

  @Input('presentationTrack')
  set presentationTrack(publication: any) {
    this.subscribe(publication);
  }

  @Input('localParticipant')
  set localParticipant(localParticipant: any) {
    this.setData(localParticipant);
  }
  private isAttachable(track: RemoteTrack): track is RemoteAudioTrack | RemoteVideoTrack {
    return !!track &&
        ((track as RemoteAudioTrack).attach !== undefined ||
            (track as RemoteVideoTrack).attach !== undefined);
  }

  private isDetachable(track: RemoteTrack): track is RemoteAudioTrack | RemoteVideoTrack {
    return !!track &&
        ((track as RemoteAudioTrack).detach !== undefined ||
            (track as RemoteVideoTrack).detach !== undefined);
  }


  constructor(private readonly renderer: Renderer2) { }

  setData(localParticipant) {
    if (!!localParticipant) {
      // detach existing tracks
      this.detachTracks();
      this.localInitial = localParticipant.identity.substring(0, 2);
      this.videoTrack = localParticipant.videoTracks;
      localParticipant.videoTracks.forEach( track => {
        this.attachRemoteTrack(track, false);
      });

    }
  }

  private detachTracks() {
    const children = this.preview.nativeElement.children;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < children.length; i++) {
      const element = children[i];
      element.remove();
    }
  }


  private attachRemoteTrack(track: any, isPresent: boolean) {
    if (this.isAttachable(track.track) || this.isAttachable(track)) {
      const tr = track.track ? track.track : track;
      const element = tr.attach();
      this.renderer.data.id = track.sid ? track.sid : track.trackSid;
      this.renderer.setStyle(element, 'width', '100%');
      this.renderer.setStyle(element, 'height', '100%');

      if (isPresent) {
        this.renderer.appendChild(this.ScreenTrack.nativeElement, element);
      } else {
        this.renderer.setStyle(element, 'transform', 'scale(-1, 1)');
        this.renderer.appendChild(this.preview.nativeElement, element);
      }

    }
  }

  private detachRemoteTrack(track: any) {
    if (this.isDetachable(track)) {
      track.detach().forEach(el => el.remove());
    }
  }

  private subscribe(publication: any) {
    if (publication !== undefined) {
      publication.on('subscribed', track => {
        this.isPreviewing = false;
        this.attachRemoteTrack(track, true);
      });
      publication.on('unsubscribed', track => {
        this.isPreviewing = true;
        this.detachRemoteTrack(track);
      });
    }
  }
}
