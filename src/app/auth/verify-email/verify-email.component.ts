import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/shared/service/auth/auth.service';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.css']
})
export class VerifyEmailComponent implements OnInit {

  token: string;
  loading: boolean;
  success: boolean;
  accoutStatus: string;
  wrongVerificationCode: boolean;
  errorMessage: string;

  constructor(
    private userService: AuthService,
    private activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {

    this.activatedRoute.queryParams.subscribe(params => {
      this.token = (params.token);
      if (this.token != null) {
        this.loading = true;
        this.userService.checkUserVerification(this.token).subscribe(res => {
          if (res.status === 'pending') {
            this.accoutStatus = res.status;
            this.verifyUserEmail(this.token);
          } else if (res.status === 'active') {
            this.loading = false;
            this.accoutStatus = res.status;
            this.errorMessage = 'This email has already been verified.';
          } else {
            this.loading = false;
            this.errorMessage = res.status;
          }
        }, (err) => {
          console.log(err.error);
          this.loading = false;
          this.errorMessage = 'Something went wrong.';
        });
      } else {
        this.loading = false;
        this.errorMessage = 'Something went wrong.';
      }
    });
  }

  verifyUserEmail(token) {
    if (token != null) {
      this.loading = true;
      this.userService.verifyUserEmail(this.token).subscribe(response => {
        if (response && response.accountStatus === 'active') {
          this.loading = false;
          this.success = true;
        } else {
          this.loading = false;
          this.errorMessage = 'Something went wrong.';
        }
      }, (err) => {
        console.log(err.error);
        this.loading = false;
        this.errorMessage = 'Something went wrong.';
      });
    }
  }

}
