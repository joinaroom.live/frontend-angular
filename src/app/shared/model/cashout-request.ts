import {Rooms} from './rooms';

export interface CashoutRequest {
    event: Rooms;
    id: number;
    requestId: number;
    amount: number;
    remark: string;
    status: string;
}
