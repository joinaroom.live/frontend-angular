import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {PaymentService} from '../../shared/service/payment.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatSnackBar} from '@angular/material/snack-bar';
import * as moment from 'moment';
import {MatPaginator} from '@angular/material/paginator';
import {Error} from '../../shared/model/error';
import {MatSort} from '@angular/material/sort';

@Component({
    selector: 'app-cashout-magement',
    templateUrl: './cash-out-magement.component.html',
    styleUrls: ['./cash-out-magement.component.css']
})
export class CashOutMagementComponent implements OnInit{

    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;

    displayedColumns: string[] = ['no', 'name', 'date', 'start', 'end', 'host', 'amount', 'action'];

    dataSource;
    loading: boolean;

    constructor(private paymentService: PaymentService,
                private snackBar: MatSnackBar) {
    }

    ngOnInit(): void {
        this.getData();
    }

    private getData() {
        this.loading = true;
        this.paymentService
            .getCashOutRequests()
            .subscribe(
                (cashoutData) => {
                    this.loading = false;
                    this.dataSource = new MatTableDataSource(cashoutData);
                    this.dataSource.sortingDataAccessor = (item, property) => {
                        switch (property) {
                            case 'date': return item.event.eventDate;
                            default: return item[property];
                        }
                    };
                    this.dataSource.sort = this.sort;
                    this.dataSource.paginator = this.paginator;
                    this.dataSource.filterPredicate = (data, filter: string)  => {
                        const accumulator = (currentTerm, key) => {
                            return this.nestedFilterCheck(currentTerm, data, key);
                        };
                        const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
                        // Transform the filter by converting it to lowercase and removing whitespace.
                        const transformedFilter = filter.trim().toLowerCase();
                        return dataStr.indexOf(transformedFilter) !== -1;
                    };
                },
                (error) => {
                    this.loading = false;
                    this.showMessage(error.message, 'my-snackbar-error');
                },
            );
    }


    applyFilter($event: KeyboardEvent) {
        const filterValue = ($event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    showMessage(message, classString) {
        this.snackBar.open(message, 'Ok',
            {
                duration: 2000,
                verticalPosition: 'top',
                panelClass: [classString]
            });
    }

    getTime(time): string {
        return moment.unix(time / 1000).format('LT');
    }

    getDate(eventDate): string {
        return moment(eventDate).local().format('ll');
    }
    authorize(id) {
        this.loading = true;
        this.paymentService
            .confirmPayout(id)
            .subscribe(
                (data) => {
                    this.loading = false;
                    this.showMessage('CAshOut was authorised successfully', 'my-snackbar-success');
                    this.getData();
                },
                (r) => {
                    console.log(r);
                    this.loading = false;
                    const error: Error = r.error;
                    this.showMessage(error.error, 'my-snackbar-error');
                },
            );
    }
    decline(id) {

    }

    private nestedFilterCheck(currentTerm, data, key) {
        if (typeof data[key] === 'object') {
            for (const k in data[key]) {
                if (data[key][k] !== null) {
                    currentTerm = this.nestedFilterCheck(currentTerm, data[key], k);
                }
            }
        } else {
            currentTerm += data[key];
        }
        return currentTerm;
    }
}

