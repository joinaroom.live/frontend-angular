// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  // baseUrl: 'https://dev.joinaroom.live',
  baseUrl: 'http://localhost:3000',
  login: '/auth/login',
  register: '/auth/register',
  forgot: '/auth/forgot/password',
  reset: '/auth/reset/password',
  checkUser: '/users/user-exists',
  checkUserVerification: '/users/check-user-verifiation',
  checkUserVerificationUrlByEmail: '/users/check-verifiation-by-email',
  verifyEmail: '/users/verify-user-email',
  userInfo: '/auth/info',
  rooms: '/events',
  myEvents: '/events/my-events',
  twilioToken: '/twilio-api/access-token',
  requestPayment: '/order/pay',
  confirmPayment: '/order/confirm-payment',
  payDetails: '/payment/details',
  getPayDetails: '/payment/payment-details',
  requestPayout: '/payment/payouts',
  confirmPayout: '/payment/confirm/payouts',
  payPalBalance: '/payment/payout/amount',
  paypalUrl: 'https://www.sandbox.paypal.com',
  firebaseConfig: {
    apiKey: 'AIzaSyBIB2CdtAd7ZkLIfU6BVNpuQHF5OE2XIT4',
    authDomain: 'join-a-room.firebaseapp.com',
    databaseURL: 'https://join-a-room.firebaseio.com',
    projectId: 'join-a-room',
    storageBucket: 'join-a-room.appspot.com',
    messagingSenderId: '587216454',
    appId: '1:587216454:web:1cf8807fc2f6d91738bc47'
  },
  merchantInfo: '/merchant/info',
  merchantLinks: '/merchant/links',
  cashOutRequest: '/approve-payments',
  allUserEvents: '/events/all-payment-events'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
