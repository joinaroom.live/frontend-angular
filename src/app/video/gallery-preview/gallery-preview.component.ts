import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter, HostListener,
  Input,
  OnInit,
  Output,
  Renderer2,
  ViewChild
} from '@angular/core';
import {
  RemoteParticipant,
  RemoteTrack,
  RemoteAudioTrack,
  RemoteVideoTrack,
  RemoteTrackPublication,
  LocalAudioTrackPublication,
  LocalVideoTrackPublication
} from 'twilio-video';
import { MuteEventData } from '../video.component';

@Component({
  selector: 'app-gallery-preview',
  templateUrl: './gallery-preview.component.html',
  styleUrls: ['./gallery-preview.component.css']
})
export class GalleryPreviewComponent implements OnInit, AfterViewInit {

  @ViewChild('preview', { static: false }) preview: ElementRef;
  @ViewChild('element', { static: false }) element: ElementRef;

  isPreviewing = false;
  localInitial: string;
  host: string;
  isHostValue: boolean;
  sid: string;
  part: any;
  participantsCount: any;
  isMute: boolean;
  currentIdentity;

  @Input('participant')
  set participant(participant: any) {
    this.part = participant;
  }
  @Input('count')
  set count(count: any) {
    this.participantsCount = count;
  }

  @Input('isHost')
  set isHost(host: boolean) {
    this.isHostValue = host;
  }

  @Input('hostIdentity')
  set hostIdentity(host: string) {
    this.host = host;
  }


  @Input('localParticipant')
  set localParticipant(localParticipant: any) {
    this.currentIdentity = localParticipant?.identity;
  }

  @Output('muted') muted = new EventEmitter<Map<string, boolean>>();
  @Output('updateMute') updateMute = new EventEmitter<Map<string, boolean>>();
  @Output('screenTrack') screenTrack = new EventEmitter<RemoteTrackPublication>();
  @Output('currentMuteState') currentMuteState = new EventEmitter();


  @Input('micOn')
  set micOn(micOn: boolean) {
    this.isMute = micOn;
  }

  private isAttachable(track: RemoteTrack): track is RemoteAudioTrack | RemoteVideoTrack {
    return !!track &&
      ((track as RemoteAudioTrack).attach !== undefined ||
        (track as RemoteVideoTrack).attach !== undefined);
  }

  private isDetachable(track: RemoteTrack): track is RemoteAudioTrack | RemoteVideoTrack {
    return !!track &&
      ((track as RemoteAudioTrack).detach !== undefined ||
        (track as RemoteVideoTrack).detach !== undefined);
  }

  private isLocal(track: any): track is LocalAudioTrackPublication | LocalVideoTrackPublication {
    return !!track;
  }


  constructor(private readonly renderer: Renderer2) { }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    if (this.part !== null) {
      let local;
      this.part.videoTracks.forEach(track => {
        local = this.isLocal(track.track);
      });
      if (local) {
        this.part.videoTracks.forEach(track => {
          this.attachLocalTrack(track);
        });
      } else {
        this.registerEvents(this.part);
      }
    }
  }

  private registerEvents(participant: RemoteParticipant) {
    this.localInitial = participant.identity.substring(0, 1);
    participant.tracks.forEach(publication => this.subscribe(publication));
    participant.on('trackPublished', publication => this.subscribe(publication));
    participant.on('trackUnpublished',
      publication => {
        if (publication && publication.track) {
          this.detachRemoteTrack(publication.track);
        }
      });
  }

  private subscribe(publication: RemoteTrackPublication | any) {
    if (publication && publication.on && publication.publishPriority !== 'high' && publication.trackName !== 'screen') {
      publication.on('subscribed', track => {
        this.attachRemoteTrack(track);
      });
      publication.on('unsubscribed', track => this.detachRemoteTrack(track));
    } else {
      if (publication.trackName === 'screen') {
        this.screenTrack.emit(publication);
      }
    }
  }

  private attachRemoteTrack(track: RemoteTrack) {
    if (track.kind === 'data') {
      this.currentMuteState.emit();
      track.on('message', data => {
        const val: MuteEventData = JSON.parse(data);
        const values = new Map<string, boolean>();
        values.set(val?.sid, val.mute);
        this.updateMute.emit(values);
      });
    }
    if (this.isAttachable(track)) {
      this.isPreviewing = true;
      const element = track.attach();
      this.renderer.data.id = track.sid;

      this.renderer.appendChild(this.preview.nativeElement, element);
    }
  }

  private detachRemoteTrack(track: RemoteTrack) {
    if (this.isDetachable(track)) {
      track.detach().forEach(el => el.remove());
      this.isPreviewing = false;
    }
  }

  private attachLocalTrack(track: any) {
    if (track.publishPriority !== 'high' && track.priority !== 'high') {
      if (this.isAttachable(track.track)) {
        this.isPreviewing = true;
        const element = track.track.attach();
        this.renderer.data.id = track.sid ? track.sid : track.trackSid;

        this.renderer.appendChild(this.preview.nativeElement, element);
      }
    }
  }

  mute(sid: string) {
    this.isMute = !this.isMute;
    const values = new Map<string, boolean>();
    values.set(sid, this.isMute);
    this.muted.emit(values);
  }

  isDisabled(): boolean {
    if (this.isHostValue) {
      return false;
    } else if (this.currentIdentity === this.part?.identity) {
      return false;
    } else if (this.currentIdentity !== this.part?.identity) {
      return true;
    }
  }
}

