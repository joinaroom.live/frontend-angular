import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashOutMagementComponent } from './cash-out-magement.component';

describe('CashoutMagementComponent', () => {
  let component: CashOutMagementComponent;
  let fixture: ComponentFixture<CashOutMagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashOutMagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashOutMagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
