    $(document).ready(function() {

      try {
        //Preloader
        $(window).load(function () {
          $("#loader").fadeOut();
          $("#mask").delay(1000).fadeOut("slow");
        });
      } catch (e) {}

      try {
        //first letter to profile image
        $(document).ready(function () {
          let intials = $('.name').text().charAt(0);
          let profileImage = $('.profile').text(intials);
        });
      } catch (e) {}

      try {
        //Adding fixed position to header
        $(document).scroll(function () {
          if ($(document).scrollTop() >= 500) {
            $('.navbar').addClass('navbar-fixed-top');
            $('html').addClass('has-fixed-nav');
          } else {
            $('.navbar').removeClass('navbar-fixed-top');
            $('html').removeClass('has-fixed-nav');
          }
        });
      } catch (e) {}

      try {
        //Home Text Slider
        $('.home-slider').flexslider({
          animation: "slide",
          directionNav: false,
          controlNav: false,
          direction: "vertical",
          slideshowSpeed: 5000,
          animationSpeed: 1000,
          smoothHeight: true,
          useCSS: false
        });
      } catch (e) {}

      try {
        //Elements animation
        $('.animated').appear(function () {
          var element = $(this);
          var animation = element.data('animation');
          var animationDelay = element.data('delay');
          if (animationDelay) {
            setTimeout(function () {
              element.addClass(animation + " visible");
              element.removeClass('hiding');
              if (element.hasClass('counter')) {
                element.children('.value').countTo();
              }
            }, animationDelay);
          } else {
            element.addClass(animation + " visible");
            element.removeClass('hiding');
            if (element.hasClass('counter')) {
              element.children('.value').countTo();
            }
          }
        }, {accY: -150});
      } catch (e) {}

      try {
        //Portfolio filters
        $('#portfolio-grid').mixitup({
          effects: ['fade', 'scale'],
          easing: 'snap'
        });
      } catch (e) {}

      try {
        //Portfolio project slider
        function initProjectSlider() {
          $('.project-slider').flexslider({
            prevText: "",
            nextText: "",
            useCSS: false,
            animation: "slide"
          });
        }
      } catch (e) {}
    });
