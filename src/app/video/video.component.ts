import {
    AfterViewInit,
    Component,
    HostListener, Inject,
    Input,
    OnInit,
    Renderer2,
    ViewChild,
    ViewContainerRef,
} from '@angular/core';
import {
    connect,
    Room,
    LocalParticipant,
    RemoteParticipant,
    Participant,
    RemoteTrack,
    LocalTrack,
    RemoteTrackPublication,
    RemoteAudioTrack,
    RemoteVideoTrack,
    ConnectOptions,
    createLocalTracks,
    isSupported,
    LocalVideoTrack,
    LocalDataTrack
} from 'twilio-video';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DialogInviteComponent } from '../roombooking/dialog-invite/dialog-invite.component';
import { FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { Rooms } from '../shared/model/rooms';
import { RoomService } from '../shared/service/room/room.service';
import { DOCUMENT } from '@angular/common';
import { Observable } from 'rxjs';
import { TwilioToken } from '../shared/model/twilio-token';
import { VideoChatService } from '../shared/service/video/videochat.service';
import fscreen from 'fscreen';
import { isMobile, removeUndefineds } from '../utils/common';
import debounce from "lodash/debounce";

export interface MuteEventData {
    sid: string;
    mute: boolean;
}

@Component({
    selector: 'app-video',
    templateUrl: './video.component.html',
    styleUrls: ['./video.component.css']
})
export class VideoComponent implements AfterViewInit, OnInit {

    constructor(
        @Inject(DOCUMENT) private document: any,
        private dialog: MatDialog,
        private readonly renderer: Renderer2,
        private snackBar: MatSnackBar,
        private roomService: RoomService,
        private routeParams: ActivatedRoute,
        private readonly videoChatService: VideoChatService) {
    }

    get participantCount() {
        return !!this.participants ? this.participants.size : 0;
    }

    get isAlone() {
        return this.participantCount === 0;
    }

    @Input() userID: number;
    @Input() roomID: number;
    form: FormGroup;
    isPresenting = false;
    isJoining: boolean;
    activeRoom: any = null;
    localInitial: string;
    // present
    stream;
    screenTrack;
    roomInfo: Rooms;
    fullScreen = false;
    speakerView = false;
    minimizePreview = false;
    showToolbar = true;
    showFullScreenBar = true;

    elem: any;

    private participants: Map<Participant.SID, RemoteParticipant>;
    dominantSpeaker: Observable<RemoteParticipant>;
    localParticipant: Observable<LocalParticipant>;
    dataTrack = new LocalDataTrack();
    dataTrackPublished: any = {};
    muteEventValue = new Map<string, boolean>();
    presentationTrack;
    isHost: boolean;

    private isAttachable(track: RemoteTrack): track is RemoteAudioTrack | RemoteVideoTrack {
        return !!track &&
            ((track as RemoteAudioTrack).attach !== undefined ||
                (track as RemoteVideoTrack).attach !== undefined);
    }

    private isDetachable(track: RemoteTrack): track is RemoteAudioTrack | RemoteVideoTrack {
        return !!track &&
            ((track as RemoteAudioTrack).detach !== undefined ||
                (track as RemoteVideoTrack).detach !== undefined);
    }

    ngOnInit() {
        this.elem = document.documentElement;
        this.routeParams.params.subscribe(params => {
            this.userID = params.user;
            if (params.id != null) {
                this.roomID = params.id;
                this.getRoom(params.id);
            }
        });
    }
    @HostListener('document:click', ['$event'])
    toggleToolBar(e: any) {
        e.stopPropagation();
        console.log(e);
        if (e.target.id === 'bottomBar') {
            this.showToolbar = true;
        } else {
            this.showToolbar = false;
        }
    }

    @HostListener('document:fullscreenchange', ['$event'])
    @HostListener('document:webkitfullscreenchange', ['$event'])
    @HostListener('document:mozfullscreenchange', ['$event'])
    @HostListener('document:MSFullscreenChange', ['$event'])
    fullScreenMode() {
        this.fullScreen = !this.fullScreen;
        if (this.fullScreen) {
            let time = 0;
            const interval = setInterval(() => {
                time++;
                if (time === 10) {
                    this.showFullScreenBar = false;
                    clearInterval(interval);
                }
            }, 1000);
        }
    }

    ngAfterViewInit(): void {
        if (isSupported) {
            this.connectRoom();
        } else {
            this.snackBar.open('This browser is not supported please visit our website on different browser', 'Ok',
                {
                    verticalPosition: 'top',
                    panelClass: ['my-snackbar']
                });
        }
    }

    private getRoom(id: number) {
        this.localInitial = 'Me';
        this.roomService.getRoomById(id)
            .subscribe(data => {
                    this.roomInfo = data;
                },
                (error) => {
                },
            );
    }

    getConnectionOptions(localTracks) {
        const connectionOptions: ConnectOptions = {
            tracks: localTracks,
            bandwidthProfile: {
                video: {
                    mode: 'collaboration',
                    maxTracks: 10,
                    dominantSpeakerPriority: 'standard',
                    renderDimensions: {
                        high: {height: 720, width: 1280},
                        standard: {height: 480, width: 640},
                        low: {height: 144, width: 176}
                    }
                }
            },
            dominantSpeaker: true,
            preferredVideoCodecs: ['H264', 'VP8'],
            // preferredVideoCodecs: [{ codec: 'VP8', simulcast: true }],
            networkQuality: {local: 1, remote: 1}
        };

        // For mobile browsers, limit the maximum incoming video bitrate to 2.5 Mbps.
        if (isMobile) {
            // tslint:disable-next-line: no-non-null-assertion
            if (connectionOptions?.bandwidthProfile?.video) {
                connectionOptions!.bandwidthProfile!.video!.maxSubscriptionBitrate = 2500000;
                connectionOptions!.bandwidthProfile!.video!.maxTracks = 5;
            }
        }

        console.log(connectionOptions);
        return removeUndefineds(connectionOptions);
    }

    connectRoom() {
        const twilioTokenRequest = {} as TwilioToken;
        twilioTokenRequest.eventId = this.roomID;
        twilioTokenRequest.userId = this.userID;

        this.videoChatService
            .getAuthToken(twilioTokenRequest)
            .subscribe((data) => {
                    createLocalTracks({
                        audio: true,
                        video: {
                            height: 720,
                            frameRate: 10,
                            width: 1280
                        }
                    }).then(localTracks => {
                        /* connect to twilio*/
                        this.isJoining = true;
                        return connect(data.twilioToken, this.getConnectionOptions(localTracks));
                    }).then(room => {
                        this.isJoining = false;
                        this.activeRoom = room;
                        this.publishDataTrack();
                        this.initialize();
                        this.registerRoomEvents();
                        this.setHost();
                        this.localParticipant = this.activeRoom.localParticipant;
                        this.localInitial = this.activeRoom.localParticipant.identity.substring(0, 1);
                        this.toaster(`Connected to Room: ${this.activeRoom.name} as "${this.activeRoom.localParticipant.identity}"`);
                        // hide toolbar after 10sec since room connected
                        this.hideToolBar();
                    }, (err) => {
                        console.error('Safari', err);
                        this.isJoining = false;
                        this.toaster('Failed to connect to room. Taking you back to our website.');
                        //window.close();
                    });
                },
                (error) => {
                    this.toaster(error.message);
                });
    }

    private hideToolBar() {
        let time = 0;
        const interval = setInterval(() => {
            time++;
            if (time === 10) {
                this.showToolbar = false;
                clearInterval(interval);
            }
        }, 1000);
    }


    getParticipants() {
        if (!!this.participants) {
            return Array.from(this.participants).map(item => item[1]);
        } else {
            return [];
        }
    }

    present() {
        if (this.activeRoom !== null) {
            if (this.isPresenting) {
                this.isPresenting = false;
                this.activeRoom.localParticipant.unpublishTrack(this.screenTrack);
            } else {
                this.isPresenting = true;
                const mediaDevices = navigator.mediaDevices as any;
                mediaDevices.getDisplayMedia({
                    video: {
                        minWidth: 1280,
                        maxWidth: 1920,
                        minHeight: 720,
                        maxHeight: 1080
                    }
                }).then(stream => {
                    this.stream = stream;
                    this.screenTrack = new LocalVideoTrack(stream.getTracks()[0], {
                        priority: 'high',
                        name: 'screen'
                    });
                    this.activeRoom.localParticipant?.publishTrack(this.screenTrack, {
                        priority: 'high',
                        name: 'screen'
                    });
                    this.screenTrack.once('stopped', (t) => {
                        this.isPresenting = false;
                        this.activeRoom.localParticipant?.unpublishTrack(this.screenTrack);
                    });
                }, (err) => {
                    console.error(err);
                    this.isPresenting = false;
                    this.toaster('Error Sharing Screen');
                });
            }
        }
    }

    invite() {
        if (this.roomInfo !== null && this.activeRoom !== null) {
            const dialogConfig = new MatDialogConfig();
            dialogConfig.autoFocus = true;
            dialogConfig.data = this.roomInfo;
            dialogConfig.panelClass = 'custom-dialog-container';
            const dialogRef = this.dialog.open(DialogInviteComponent, dialogConfig);
            dialogRef.afterClosed().subscribe(
                data => console.log('Dialog output:', data)
            );
        }
    }

    toaster(message) {
        this.snackBar.open(message, 'Ok',
            {
                duration: 2000,
                verticalPosition: 'top',
                panelClass: ['my-snackbar']
            });
    }

    getUserInitial(name): string {
        return name.toString().substring(0, 2);
    }

    onFullScreenToggle(): void {
        if (fscreen.fullscreenElement !== null) {
            console.log('Fullscreen mode turned ON');
        } else {
            console.log('Fullscreen mode turned OFF');
        }
    }

    openFullscreen() {
        try {
            if (!this.elem) {
                this.elem = document.documentElement || document;
            }

            if (!this.fullScreen) {
                if (fscreen.fullscreenEnabled) {
                    fscreen.addEventListener('fullscreenchange', this.onFullScreenToggle, false);
                    fscreen.requestFullscreen(this.elem);
                }
            } else {
                fscreen.exitFullscreen();
            }
        } catch (e) {
            console.error(e);
        }
    }

    toggleView() {
        this.speakerView = !this.speakerView;
        if (!this.speakerView) {
            const speakerViewNode: any = document.querySelector('.speakerViewContainer video');
            speakerViewNode.style.height = '100vh';
        }

        setTimeout(() => {
            this.restartVideoTrack();
        }, 0);
    }

    togglePreviews() {
        this.minimizePreview = !this.minimizePreview;
        setTimeout(() => {
            this.restartVideoTrack();
        }, 0);
    }


    private registerRoomEvents() {
        this.activeRoom
            .on('disconnected', (room: Room) => {
                console.log('disconnected');
                room.localParticipant.tracks.forEach((publication) => this.detachLocalTrack(publication.track));
            })
            .on('participantConnected', (participant: RemoteParticipant) => {
                console.log('participantConnected');
                this.add(participant);
            })
            .on('participantDisconnected', (participant: RemoteParticipant) => {
                console.log('participantDisconnected');
                if (this.roomInfo.user.username === participant.identity){
                    this.toaster('This event is ended by the host.');
                    setTimeout(() => {
                        this.leaveRoom();
                    }, 5000);
                }
                this.remove(participant);
            })
            .on('dominantSpeakerChanged', (dominantSpeaker) => {
                this.loudest(dominantSpeaker);
            });
    }

    private detachLocalTrack(track: LocalTrack) {
        if (this.isDetachable(track)) {
            track.detach().forEach(el => el.remove());
        }
    }

    leaveRoom() {
        try {
            if (this.activeRoom) {
                this.participants.delete(this.activeRoom.localParticipant.sid);
                console.log(this.activeRoom.participants);
                this.activeRoom.disconnect();
                this.activeRoom = null;
            }
            this.participants.clear();
        } catch (e) {
            this.activeRoom = null;
            console.error(e);
        }
        window.close();
    }

    initialize() {
        // initial mute status
        const participants = this.activeRoom.participants;
        if (participants) {
            participants.forEach(participant => {
                this.muteEventValue.set(participant.sid, true);
            });
        }
        this.muteEventValue.set(this.activeRoom.localParticipant.sid, true);
        // set participant list
        this.participants = participants;
        this.participants.set(this.activeRoom.localParticipant.sid, this.activeRoom.localParticipant);

        const debouncedRecalculateLayout = debounce(this.recalculateLayout, 50);
        window.addEventListener("resize", debouncedRecalculateLayout);
        debouncedRecalculateLayout();
    }

    add(participant: RemoteParticipant) {
        if (this.participants && participant) {
            this.muteEventValue.set(participant.sid, true);
            this.participants.set(participant.sid, participant);
            // broadcast local user mute status
            this.currentMuteState();
        }
    }

    currentMuteState() {
        this.muteEventValue.forEach((value, key, map) => {
            this.muteEventValue.set(key, value);
            if (this.activeRoom?.localParticipant?.sid === key) {
                const muteEventData = {} as MuteEventData;
                muteEventData.sid = key;
                muteEventData.mute = value;
                this.sendMessage(muteEventData);
            }
        });
    }

    remove(participant: RemoteParticipant) {
        if (this.participants && this.participants.has(participant.sid)) {
            this.participants.delete(participant.sid);
            document.getElementById(participant.sid + 'sv')?.remove();
            document.getElementById(participant.sid + 'gv')?.remove();
        }
    }

    loudest(participant: RemoteParticipant) {
        if (participant !== null) {
            this.dominantSpeaker = participant;
        }
    }

    private publishDataTrack() {
        this.activeRoom.localParticipant.publishTrack(this.dataTrack);
        this.dataTrackPublished.promise = new Promise((resolve, reject) => {
            this.dataTrackPublished.resolve = resolve;
            this.dataTrackPublished.reject = reject;
        });

        this.activeRoom.localParticipant.on('trackPublished', publication => {
            if (publication.track === this.dataTrack) {
                this.dataTrackPublished.resolve();
                console.log('track published');
                // broadcast local user mute status
                this.currentMuteState();
            }
        });

        this.activeRoom.localParticipant.on('trackPublicationFailed', (error, track) => {
            if (track === this.dataTrack) {
                this.dataTrackPublished.reject(error);
            }
        });
    }

    updateMute($event: Map<string, boolean>) {
        console.log(this.activeRoom?.localParticipant?.sid)
        $event.forEach((value, key, map) => {
            this.muteEventValue.set(key, value);
            if (this.activeRoom?.localParticipant?.sid === key) {
                this.disableEnableVoice(value);
            }
        });
    }

    muteEvent($event: Map<string, boolean>) {
        $event.forEach((value, key, map) => {
            this.muteEventValue.set(key, value);
            if (this.activeRoom?.localParticipant?.sid === key) {
                this.disableEnableVoice(value);
            }
            const muteEventData = {} as MuteEventData;
            muteEventData.sid = key;
            muteEventData.mute = value;
            this.sendMessage(muteEventData);
        });
    }

    toggleVoice(participant: any) {
        if (this.activeRoom !== null) {
            const values = new Map<string, boolean>();
            values.set(participant?.sid, !this.muteEventValue.get(participant?.sid));
            this.muteEvent(values);
        }
    }


    disableEnableVoice(micOn: boolean) {
        if (this.activeRoom != null) {
            if (micOn) {
                this.activeRoom.localParticipant.audioTracks.forEach(publication => {
                    publication.track.enable();
                });
            } else {
                this.activeRoom.localParticipant.audioTracks.forEach(publication => {
                    publication.track.disable();
                });
            }
        }
    }

    sendMessage(event: MuteEventData) {
        this.dataTrackPublished.promise.then(() => {
            this.dataTrack.send(`{"sid": "${event.sid}", "mute": ${event.mute}}`);
        });
    }


    hostIdentity(identity: string): string {
        if (this.roomInfo != null && this.roomInfo.user.username === identity) {
            return '(Host)';
        } else {
            return '';
        }
    }

    private setHost() {
        if (this.roomInfo !== null) {
            this.isHost = this.roomInfo?.user?.username === this.activeRoom.localParticipant.identity;
        }
    }

    isDisabled(participant): boolean {
        if (this.isHost) {
            return false;
        } else if (this.activeRoom.localParticipant.identity === participant?.identity) {
            return false;
        } else if (this.activeRoom.localParticipant.identity !== participant?.identity) {
            return true;
        }
    }

    setScreenTrack($event: RemoteTrackPublication) {
        this.presentationTrack = $event;
        this.speakerView = false;
    }

    restartVideoTrack() {
        this.activeRoom.localParticipant.videoTracks.forEach(publication => {
            if (publication.track.kind === 'video') {
                publication.track.restart();
            }
        });
    }



    recalculateLayout() {
        const gallery = document.getElementById("video-gallery");
        const aspectRatio = 16 / 9;
        const screenWidth = document.body.getBoundingClientRect().width;
        const screenHeight = document.body.getBoundingClientRect().height;
        
        const videoCount = document.getElementsByTagName("video").length;

        // or use this nice lib: https://github.com/fzembow/rect-scaler
        function calculateLayout(
            containerWidth: number,
            containerHeight: number,
            videoCount: number,
            aspectRatio: number
        ): { width: number; height: number; cols: number } {
            let bestLayout = {
                area: 0,
                cols: 0,
                rows: 0,
                width: 0,
                height: 0
            };

            // brute-force search layout where video occupy the largest area of the container
            for (let cols = 1; cols <= videoCount; cols++) {
                const rows = Math.ceil(videoCount / cols);
                const hScale = containerWidth / (cols * aspectRatio);
                const vScale = containerHeight / rows;
                let width;
                let height;
                if (hScale <= vScale) {
                    width = Math.floor(containerWidth / cols);
                    height = Math.floor(width / aspectRatio);
                } else {
                    height = Math.floor(containerHeight / rows);
                    width = Math.floor(height * aspectRatio);
                }
                const area = width * height;
                if (area > bestLayout.area) {
                    bestLayout = {
                        area,
                        width,
                        height,
                        rows,
                        cols
                    };
                }
            }
            return bestLayout;
        }

        const { width, height, cols } = calculateLayout(
            screenWidth,
            screenHeight,
            videoCount,
            aspectRatio
        );

        gallery.style.setProperty("--width", width + "px");
        gallery.style.setProperty("--height", height + "px");
        gallery.style.setProperty("--cols", cols + "");
    }
}
