import {Component, EventEmitter, Inject, isDevMode, OnInit, Output, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RoomService} from '../../shared/service/room/room.service';
import {MatDatepicker} from '@angular/material/datepicker';
import {Rooms} from '../../shared/model/rooms';
import {AngularFireStorage, AngularFireUploadTask} from '@angular/fire/storage';
import {finalize} from 'rxjs/operators';
import * as moment from 'moment';
import {MatSnackBar} from '@angular/material/snack-bar';
import {NgxMaterialTimepickerComponent} from 'ngx-material-timepicker';

@Component({
    selector: 'app-dialog-schedule',
    templateUrl: './dialog-schedule.component.html',
    styleUrls: ['./dialog-schedule.component.css']
})

export class DialogScheduleComponent implements OnInit {

    @ViewChild('startDatepicker') startDatepicker: MatDatepicker<Date>;
    @ViewChild('endDatePicker') endDatepicker: MatDatepicker<Date>;

    @Output() dropped = new EventEmitter<FileList>();
    @Output() hovered = new EventEmitter<boolean>();

    form: FormGroup;
    loading = false;
    isHovering: boolean;
    task: AngularFireUploadTask;
    downloadURL;
    fileError;

    /*edit values*/
    actionText: string;
    eventName: string;
    price: number;
    maxNoPeople: number;
    description: string;
    startTime: string;
    endTime: string;
    eventDate: Date;
    minimumDate: Date;
    minimumPrice: number;
    defaultStartTime;
    noImage = false;

    constructor(private dialog: MatDialog,
                private fb: FormBuilder,
                private snackBar: MatSnackBar,
                private storage: AngularFireStorage,
                private dialogRef: MatDialogRef<DialogScheduleComponent>,
                private roomService: RoomService,
                @Inject(MAT_DIALOG_DATA) public room: Rooms) {
    }

    ngOnInit(): void {
        this.minimumDate = new Date();
        this.defaultStartTime = moment().format('LT');
        if (this.room != null) {
            this.actionText = 'Update';
            this.downloadURL = this.room.imageUrl;
            this.eventName = this.room?.eventName;
            this.price = this.room?.price;
            this.maxNoPeople = this.room?.maxNoPeople;
            this.description = this.room?.description;
            this.eventDate = this.room?.eventDate;
            this.startTime = moment.unix(this.room?.startTime / 1000).format('HH:mm');
            this.endTime = moment.unix(this.room?.endTime / 1000).format('HH:mm');
        } else {
            this.actionText = 'Save';
        }
        if (isDevMode()) {
            this.minimumPrice = 0;
        } else {
            this.minimumPrice = 0;
        }
        this.form = this.fb.group({
            eventName: [this.eventName, Validators.required],
            price: [this.price, [Validators.required, Validators.min(this.minimumPrice), Validators.max(9999), Validators.maxLength(4)]],
            maxNoPeople: [this.maxNoPeople, [Validators.required, Validators.min(2), Validators.max(50)]],
            description: [this.description, Validators.required],
            eventDate: [this.eventDate, [Validators.required]],
            startTime: [this.startTime, Validators.required],
            endTime: [this.endTime, Validators.required],
        });

    }

    save() {
        this.sendData();
        // make sure user is signed in
    }

    sendData() {
        if (this.form.invalid) {
            return;
        }
        const room: Rooms = this.form.value;
        if (this.room != null) {
            room.id = this.room.id;
        }
        room.eventDate = this.form.value.eventDate;
        const endDateValue = new Date(new Date(room.eventDate).toDateString() + ' ' + this.form.value.endTime);
        if (this.startTime?.substr(this.startTime.length - 2) === 'PM'
            && this.endTime?.substr(this.endTime.length - 2) === 'AM') {
            endDateValue.setDate(endDateValue.getDate() + 1);
        }

        room.startTime = new Date(new Date(room.eventDate).toDateString() + ' ' + this.startTime).getTime();
        room.endTime = endDateValue.getTime();

        if (room.startTime / 1000 < moment().unix()) {
            this.form.controls.startTime.setErrors({incorrect: true});
        }
        if (room.startTime > room.endTime) {
            this.form.controls.endTime.setErrors({incorrect: true});
        }
        if (this.downloadURL != null) {
            this.loading = true;
            room.imageUrl = this.downloadURL;
            this.roomService
                .sendRoomDetails(room)
                .subscribe(
                    (data) => {
                        this.dialogRef.close(data);
                        this.loading = false;
                    },
                    (error) => {
                        this.loading = false;
                    },
                );
        } else {
            this.noImage = true;
        }
    }

    close() {
        this.dialogRef.close(false);
    }

    toggleHover(event: boolean) {
        this.isHovering = event;
    }

    onDrop(file: FileList) {
        const image = file[0];
        this.validateFile(image);
    }

    private validateFile(image: File) {
        const fileSizeInKB = Math.round(image.size / 1024);
        if (fileSizeInKB >= 2048) {
            this.fileError = true;
        } else {
            this.fileError = false;
            this.startUpload(image);
        }
    }

    upload(file: any) {
        const image = file.files[0];
        this.validateFile(image);
    }

    startUpload(image: File) {
        this.loading = true;
        const path = `joinARoom/${Date.now()}_${image.name}`;
        const ref = this.storage.ref(path);
        this.task = this.storage.upload(path, image);
        this.storage.upload(path, image).snapshotChanges().pipe(
            finalize(() => {
                ref.getDownloadURL().subscribe((url) => {
                    this.downloadURL = url;
                    this.loading = false;
                    this.noImage = false;
                });
            })
        ).subscribe();
    }

    onEndTimeSet($event: string) {
        this.endTime = $event;
        const startValue = this.startTime?.substr(this.form.value.startTime.length - 2);
        const endValue = $event?.substr($event.length - 2);
        const defaultDate = new Date(new Date().toDateString() + ' ' + $event);
        const selectedDate = new Date(new Date(this.form.value.eventDate).toDateString() + ' ' + $event);
        const startDate = new Date(
            new Date(this.form.value.eventDate ? this.form.value.eventDate : new Date()).toDateString()
            + ' ' + this.form.value.startTime).getTime();

        if (startValue === 'PM' && endValue === 'AM') {
            defaultDate.setDate(defaultDate.getDate() + 1);
            selectedDate.setDate(defaultDate.getDate() + 1);
        }

        const endDate = this.form.value.eventDate ? selectedDate.getTime() : defaultDate.getTime();
        if (endDate < startDate) {
            console.log('Invalida end');
            this.form.controls.endTime.setErrors({incorrect: true});
        }
    }

    onStartTimeSet($event: string, endTime: NgxMaterialTimepickerComponent) {
        this.startTime = $event;
        const time = new Date(new Date().toDateString() + ' ' + $event);
        time.setHours(time.getHours() + 1);
        const value = moment(new Date(time)).format('LT');
        this.form.patchValue({endTime: value});
        endTime.defaultTime = value;
        this.endTime = value;
    }
}
