import {NgModule, OnInit} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AccountComponent} from '../roombooking/account/account.component';
import {RoomsComponent} from '../roombooking/rooms/rooms.component';
import {MyroomsComponent} from '../roombooking/myrooms/myrooms.component';
import {PaymentRedirectComponent} from '../roombooking/payment-redirect/payment-redirect.component';
import {MainComponent} from '../main/main.component';
import {HomeComponent} from '../home/home.component';
import {SetupComponent} from '../roombooking/setup/setup.component';
import {PasswordComponent} from '../auth/password/password.component';
import {AuthActivateGuard} from '../auth-activate.guard';
import { PartnerCallbackComponent } from '../roombooking/partner-callback/partner-callback.component';
import {CashOutMagementComponent} from '../admin/cashout-magement/cash-out-magement.component';
import {TermsAndConditionsComponent} from '../terms-and-conditions/terms-and-conditions.component';
import {PrivacyPolicyComponent} from '../privacy-policy/privacy-policy.component';
import {VideoComponent} from '../video/video.component';
import { VerifyEmailComponent } from '../auth/verify-email/verify-email.component';

const routes: Routes = [
    {path: '*', redirectTo: 'home'},
    {path: 'home', component: HomeComponent},
    {path: 'terms-conditions', component: TermsAndConditionsComponent},
    {path: 'privacy-policy', component: PrivacyPolicyComponent},
    {path: 'video/:user/:id', component: VideoComponent},
    {path: '', component: MainComponent, children: [
            {path: '', pathMatch: 'full', redirectTo: 'my-rooms'},
            {path: 'my-rooms', component: MyroomsComponent, canActivate: [AuthActivateGuard]},
            {path: 'account', component: SetupComponent, canActivate: [AuthActivateGuard]},
            {path: 'all-rooms', component: RoomsComponent, canActivate: [AuthActivateGuard]},
            {path: 'rooms/:id', component: AccountComponent},
            {path: 'payment', component: PaymentRedirectComponent, canActivate: [AuthActivateGuard]},
            {path: 'password-reset', component: PasswordComponent},
            {path: 'partner-callback', component: PartnerCallbackComponent},
            {path: 'verify-email', component: VerifyEmailComponent},
            {path: 'admin', component: CashOutMagementComponent}
        ]
    }
];


@NgModule({
    imports: [RouterModule.forRoot(routes, {anchorScrolling: 'enabled', onSameUrlNavigation: 'reload'})],
    exports: [RouterModule]
})
export class AppRoutingModule {
}


