import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {AuthService} from '../../shared/service/auth/auth.service';
import {RoomService} from '../../shared/service/room/room.service';
import {User} from '../../shared/model/user';
import {Rooms} from '../../shared/model/rooms';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PaymentService} from '../../shared/service/payment.service';
import {PayDetails} from '../../shared/model/pay-details';
import {MatSnackBar} from '@angular/material/snack-bar';
import {PayPalBalance} from '../../shared/model/pay-pal-balance';
import {CashOutComponent} from '../cash-out/cash-out.component';
import {Error} from '../../shared/model/error';
import {ConfirmPayoutComponent} from '../confirm-payout/confirm-payout.component';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import * as moment from 'moment';

@Component({
    selector: 'app-setup',
    templateUrl: './setup.component.html',
    styleUrls: ['./setup.component.css']
})
export class SetupComponent implements OnInit{

    displayedColumns: string[] = ['no', 'room', 'eventDate', 'members', 'maxMembers', 'price', 'paid', 'balance', 'action'];
    dataSource;

    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;

    currentUser: User;
    loading: boolean;
    total = 0;
    form: FormGroup;
    changePayEmail: boolean;
    payDetails: PayDetails;
    payPalBalance: PayPalBalance;
    email: string;
    currency: number;

    constructor(private dialog: MatDialog,
                private fb: FormBuilder,
                private snackBar: MatSnackBar,
                private authService: AuthService,
                private roomService: RoomService,
                private paymentService: PaymentService) {
        this.authService.currentUser$.subscribe((data) => (this.currentUser = data));
    }
    ngOnInit(): void {
        this.loading = true;
        this.getRooms();
        this.getPayDetails();
        this.getBalance();
        this.loading = false;
        this.form = this.fb.group({
            paymentEmail: [this.email, Validators.compose([Validators.required, Validators.email])],
        });
        this.changePayEmail = false;

        console.log(this.paginator, this.sort);
    }
    private getRooms() {
        this.loading = true;
        this.roomService
            .getAllRoomByUser()
            .subscribe(
                (data) => {
                    const list = [];
                    data.forEach( cat => {
                        const rooms = cat.events.filter(room => room.user.id === this.currentUser.sub);
                        rooms.forEach( userRoom => {
                            list.push(userRoom);
                        });
                    });
                    this.setDataSource(list);
                    this.calculateTotal(list);
                    this.loading = false;
                },
                t => {
                    this.loading = false;
                    const error: Error = t.error;
                    this.showMessage(error.error);
                },
            );
    }
    private getPayDetails() {
        this.loading = true;
        this.paymentService
            .getPayDetails()
            .subscribe(
                (data) => {
                    this.setPaymentDetails(data);
                },
                r => {
                    this.loading = false;
                    const error: Error = r.error;
                    this.showMessage(error.error);
                },
            );
    }

    private setPaymentDetails(data: PayDetails) {
        if (data !== null) {
            this.payDetails = data;
            this.loading = false;
            this.email = this.payDetails?.paymentEmail;
            this.currency = this.payDetails?.currency;
        }
    }

    private calculateTotal(data: Rooms[]) {
        data.forEach(room => {
            const total = room.price * room.subscribers.length;
            this.total += total;
        });

    }

    private getBalance() {
        this.loading = true;
        this.paymentService
            .getPayPalBalance()
            .subscribe(
                (data) => {
                    this.payPalBalance = data;
                    this.loading = false;
                },
                r => {
                    this.loading = false;
                    const error: Error = r.error;
                    this.showMessage(error.error);
                },
            );
    }
    savePayPalEmail() {
        if (this.form.invalid) {
            return;
        }
        const  payDetails: PayDetails = this.form.value;
        payDetails.currency = 1;
        this.loading = true;
        this.paymentService
            .savePayDetails(payDetails)
            .subscribe(
                (data) => {
                    this.payDetails = data;
                    this.loading = false;
                    this.changePayEmail = false;
                },
                r => {
                    this.loading = false;
                    const error: Error = r.error;
                    this.showMessage(error.error);
                },
            );
    }

    showLoginDialog() {

    }

    showMessage(message) {
        this.snackBar.open(message, 'Ok',
            {
                duration: 2000,
                verticalPosition: 'top',
                panelClass: ['my-snackbar-error']
            });
    }

    showCashOutDialog(room) {
        if (this.payDetails !== undefined) {
            const dialogConfig = new MatDialogConfig();
            dialogConfig.disableClose = true;
            dialogConfig.autoFocus = true;
            dialogConfig.panelClass = 'custom-dialog-container2';
            dialogConfig.data = room;
            dialogConfig.width = '300px';
            const dialogRef = this.dialog.open(CashOutComponent, dialogConfig);
            dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    this.showConfirmDialog();
                }
            });
        } else {
            this.showMessage('Please set up a PayPal email first');
        }
    }


    private showConfirmDialog() {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.panelClass = 'custom-dialog-container';
        const dialogRef = this.dialog.open(ConfirmPayoutComponent, dialogConfig);
        dialogRef.afterClosed().subscribe(result => {
            this.total = 0;
            this.getBalance();
            this.getRooms();
        });
    }

    applyFilter($event: KeyboardEvent) {
        const filterValue = ($event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    private setDataSource(list: any[]) {
        this.dataSource = new MatTableDataSource(list);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort =  this.sort;
        this.dataSource.filterPredicate = (data, filter: string)  => {
            const accumulator = (currentTerm, key) => {
                return this.nestedFilterCheck(currentTerm, data, key);
            };
            const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
            // Transform the filter by converting it to lowercase and removing whitespace.
            const transformedFilter = filter.trim().toLowerCase();
            return dataStr.indexOf(transformedFilter) !== -1;
        };
    }

    private nestedFilterCheck(currentTerm, data, key) {
        if (typeof data[key] === 'object') {
            for (const k in data[key]) {
                if (data[key][k] !== null) {
                    currentTerm = this.nestedFilterCheck(currentTerm, data[key], k);
                }
            }
        } else {
            currentTerm += data[key];
        }
        return currentTerm;
    }

    formatDate(eventDate: any): string {
        return moment(eventDate).local().format('ll');
    }
}
