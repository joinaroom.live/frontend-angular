import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { AuthService } from '../../shared/service/auth/auth.service';
import { first } from 'rxjs/operators';
import { Error } from '../../shared/model/error';
import { RegisterComponent } from '../register/register.component';
import { ResetComponent } from '../reset/reset.component';
import { Router } from '@angular/router';
import { Rooms } from '../../shared/model/rooms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {

    form: FormGroup;
    loading = false;
    hide = true;
    requestError = {} as Error;

    constructor(
        private fb: FormBuilder,
        private authService: AuthService,
        private dialog: MatDialog,
        private router: Router,
        private snackBar: MatSnackBar,
        private dialogRef: MatDialogRef<LoginComponent>,
        @Inject(MAT_DIALOG_DATA) public room: Rooms) {

    }

    ngOnInit() {
        this.form = this.fb.group({
            email: ['', Validators.compose([Validators.required, Validators.email])],
            password: ['', Validators.required],
        });

    }

    close() {
        this.dialogRef.close(false);
    }

    onSubmit() {
        if (this.form.invalid) {
            return;
        }

        const email = this.form.value.email.toLowerCase();
        const password = this.form.value.password;

        this.loading = true;
        this.requestError  = {} as Error;
        this.authService.checkUserVerificationByEmail(email).subscribe(res => {
            if (res.status === 'pending') {
                this.requestError = {
                    status: 'inactive',
                    error: 'Email has not been verified yet'
                };
                this.loading = false;
            } else if (res.status === 'active') {
                this.authService
                    .login(email, password)
                    .pipe(first())
                    .subscribe(
                        (data) => {
                            this.dialogRef.close(true);
                            this.loading = false;
                        },
                        (error) => {
                            this.requestError = error.error;
                            console.log(this.requestError, this.requestError.error);
                            this.loading = false;
                        },
                    );
            } else {
                this.requestError = {
                    status: 'not found',
                    error: 'User not exist'
                };
                this.loading = false;
            }
        }, (err) => {
            console.log(err.error);
            this.loading = false;
            this.requestError = {
                status: 'error',
                error: 'Something went wrong'
            };
        });

    }

    joinNow() {
        this.showDialog(true);
    }

    reset() {

        this.showDialog(false);
    }

    showDialog(isRegister: boolean) {
        this.dialogRef.close();
        const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        if (window.innerWidth < 576) {
            dialogConfig.panelClass = 'my-full-screen-dialog';
        } else {
            dialogConfig.panelClass = 'custom-dialog-container';
        }
        if (isRegister) {
            const matDialogRef = this.dialog.open(RegisterComponent, dialogConfig);

            matDialogRef.afterClosed().subscribe(result => {
                if (result) {

                    this.snackBar.open('Your registration was successful', 'Ok', {
                        duration: 3000
                    });
                }
            });
        } else {
            this.dialog.open(ResetComponent, dialogConfig);
        }
    }
}
